package com.jasomo.hitbox;

import com.jasomo.hitbox.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by jason on 10/19/2016.
 */
public class HitboxPlugin extends JavaPlugin{
    private static HitboxPlugin instance;
    private PluginManager pm;
    @Override
    public void onEnable() {
        instance = this;
        pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new Headshot(),this);
        pm.registerEvents(new Bodyshot(),this);
        pm.registerEvents(new Legshot(),this);
    }

    @Override
    public void onDisable() {
        instance = null;
    }
}