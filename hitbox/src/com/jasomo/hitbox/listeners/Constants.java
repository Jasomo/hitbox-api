package com.jasomo.hitbox.listeners;

/**
 * Created by jason on 10/19/2016.
 */
public enum Constants {
    HEAD(1.59,2),BODY(1.59/2,1),LEGS(0,1.5);
    public final double constant;
    public final double damage;
    Constants(double constant,double damage) {
        this.constant = constant;
        this.damage=damage;
    }
    public double getValue(){
        return constant;
    }
    public double getDamage(){return damage;}
}
