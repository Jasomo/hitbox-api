package com.jasomo.hitbox.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;


/**
 * Created by jason on 10/19/2016.
 */
public class Headshot implements Listener{
    private static String header = ChatColor.GOLD + "" +ChatColor.BOLD + "[Headshot] " + ChatColor.RESET ;
    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e){
        Projectile p = (Projectile) e.getDamager();
        if(! (p.getShooter() instanceof Player)) {
            return;
        }

        Entity shot = e.getEntity();
        EntityType type = shot.getType();
        Player ph = (Player) p.getShooter();
        double playerY = p.getLocation().getY();
        double shotY = shot.getLocation().getY();
        boolean headshot = (Math.abs(playerY-shotY)) > Constants.HEAD.getValue();
        if(headshot){
            Player player = (Player) p.getShooter();
            StringBuilder m = new StringBuilder(header+player.getDisplayName()+" got a headshot on ");
                e.setDamage(e.getDamage()*Constants.HEAD.getDamage());
            if(shot instanceof Player){
                m.append(((Player) shot).getDisplayName());
            }
            Bukkit.broadcastMessage(m.toString());
    }
    }
}

