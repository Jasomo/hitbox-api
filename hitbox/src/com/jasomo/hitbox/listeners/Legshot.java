package com.jasomo.hitbox.listeners;

/**
 * Created by jason on 10/19/2016.
 */
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by jason on 10/19/2016.
 */
public class Legshot implements Listener{
    private static String header = ChatColor.GOLD + "" +ChatColor.BOLD + "[Legshot] " + ChatColor.RESET ;
    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e){
        Projectile p = (Projectile) e.getDamager();
        if(! (p.getShooter() instanceof Player)) {
            return;
        }

        Entity shot = e.getEntity();
        EntityType type = shot.getType();
        Player ph = (Player) p.getShooter();
        double playerY = p.getLocation().getY();
        double shotY = shot.getLocation().getY();
        boolean Legshot = ((Math.abs(playerY-shotY)) > Constants.LEGS.getValue() )&&((Math.abs(playerY-shotY) < Constants.BODY.getValue()));
        if(Legshot){
            Player player = (Player) p.getShooter();
            StringBuilder m = new StringBuilder(header+player.getDisplayName()+" got a legshot on ");
            e.setDamage(e.getDamage()*Constants.LEGS.getDamage());

            if(shot instanceof Player){
                Player pa = (Player) shot;
                m.append((pa.getDisplayName()));
                pa.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,500,1));
            }
            Bukkit.broadcastMessage(m.toString());
        }
    }
}